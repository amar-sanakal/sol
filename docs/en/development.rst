.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 31 mar 2014 19:37:57 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

=============
 Development
=============

SoL development is carried on in a git__ repository__ on Bitbucket__.

You can visit the `latest changes`__ page to see what's happened recently.

If you are a developer, you are more than welcome to `fork it`__ and adapt or improve it to fit
your needs, and I will happily integrate back the changes you may contribute.

Alternatively you can translate it into another language, using the `online Weblate service`__.

There is also some `technical documentation`__ automatically extracted from the sources.

__ http://git-scm.com/
__ https://bitbucket.org/lele/sol/
__ https://bitbucket.org/
__ https://bitbucket.org/lele/sol/commits/all
__ https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo%2C+Compare+Code%2C+and+Create+a+Pull+Request
__ https://hosted.weblate.org/projects/sol/
__ ../index.html
