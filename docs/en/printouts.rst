.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   gio 06 nov 2008 14:48:21 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2010, 2014, 2016 Lele Gaifax
..

===========
 Printouts
===========

.. hint:: Since version 3.33 almost all printouts show a QRCode__ on the first page containing
          an hyperlink to the corresponding `Lit Page`. The QRCode can be either recognized
          with one of the many freely available app on a smartphone, or simply clicked when
          viewing the PDF on a traditional computer.

          The QRCode **does not appear** when you use the ``localhost`` address to access
          your local instance of SoL, though. If you want it, you must use the IP address of
          your machine instead: when possible, SoL prints it out when the instance starts::

            Your IP address seems to be 192.168.1.100
            Starting server in PID 8131.
            serving on http://0.0.0.0:6996

          So instead of using the URL ``http://localhost:6996/`` to access SoL, use
          ``http://192.168.1.100:6996/`` and the QRCode should appear and be usable by other
          devices connected to your local LAN.

__ https://en.wikipedia.org/wiki/QR_code

.. _participants:

Participants
============

.. figure:: participants.png


.. _badges:

Badges
======

.. figure:: finalbadges.png


.. _scorecards:

Scorecards
==========

.. figure:: scorecards.png


.. _results:

Results
=======

.. figure:: results.png


.. _ranking:

Ranking
=======

.. figure:: ranking.png

.. figure:: natranking.png
