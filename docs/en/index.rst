.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 10 nov 2008 12:32:25 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2009, 2010, 2013, 2014 Lele Gaifax
..

=============
 User manual
=============

.. toctree::
   :maxdepth: 4

   userinterface
   printouts
   appendixes

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
