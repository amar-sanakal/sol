.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mer 09 lug 2014 10:12:29 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2010, 2013, 2014 Lele Gaifax
..

Entities
========

.. toctree::
   :maxdepth: 2

   championships
   clubs
   competitors
   matches
   mergedplayers
   players
   rates
   ratings
   tourneys
