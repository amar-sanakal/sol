.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mer 25 dic 2013 14:06:23 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2013, 2014 Lele Gaifax
..

===================================================
 :mod:`sol.i18n` -- Internationalization utilities
===================================================

.. automodule:: sol.i18n
   :members:
