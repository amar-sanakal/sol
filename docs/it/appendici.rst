.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 28 gen 2014 10:53:23 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

===========
 Appendici
===========

.. toctree::

   regole
   dedica
   sviluppo
