.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 04 feb 2014 09:13:17 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

.. _correzione concorrenti:

Correzione concorrenti
~~~~~~~~~~~~~~~~~~~~~~

.. index::
   pair: Correzione concorrenti; Tornei

È già successo, accadrà di nuovo: qualcuno ha comunicato il nome
sbagliato, o è stato frainteso o quant'altro, fatto sta che uno degli
iscritti a un torneo è la persona sbagliata e bisogna correggere
manualmente la lista dei concorrenti.

.. important:: Il problema può essere corretto in qualsiasi momento,
               anche dopo aver effettuato la premiazione. La modifica
               però dovrebbe essere fatta **prima** di diffondere i
               dati del torneo ad altre istanze di SoL, altrimenti la
               medesima correzione dovrà essere rieffettuata
               manualmente su ciascuna di esse.

Con il pulsante :guilabel:`Concorrenti` della finestra :ref:`gestione
tornei` si ottiene la solita griglia di gestione dei concorrenti di un
particolare torneo. Non è possibile inserire o togliere giocatori (usa
le funzionalità del :ref:`pannello concorrenti` per quello), ma si può
sostituire qualsiasi giocatore con qualsiasi altro.

.. warning:: Dal momento che la maschera effettua pochissimi
             controlli, è necessario usare cautela controllando due
             volte prima di confermare le modifiche. Ad esempio, i
             combo consentono di inserire lo *stesso* giocatore due
             volte, cosa che non sarà accettata dal database e causerà
             quindi un errore.
