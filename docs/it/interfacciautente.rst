.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 11 nov 2008 23:08:37 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2009, 2010, 2013, 2014 Lele Gaifax
..

====================
 Interfaccia utente
====================

Ho cercato di mantenere l'interazione con l'utente la più semplice possibile, focalizzata sulla
tipica gestione di un torneo: dato un :ref:`club <gestione club>`, selezionato il
:ref:`campionato <gestione campionati>`, si crea un nuovo :ref:`torneo <gestione tornei>` e poi

1. si inseriscono i partecipanti, selezionandoli tra i :ref:`giocatori <gestione giocatori>` ed
   eventualmente organizzandoli in squadre

   .. index:: Fantasma

   .. note:: Quando il numero di partecipanti è *dispari*, SoL gestisce **automaticamente** la
             presenza di un `giocatore fantasma`__, contro il quale si vince sempre per
             **25—0** *a tavolino* (per questo motivo, i cartellini di gioco relativi a queste
             fantomatiche partite non vengono neppure stampati). In casi particolari
             l'effettivo punteggio assegnato al giocatore può essere cambiato nella
             maschera di :ref:`inserimento e modifica torneo`.

2. eventualmente si stampano le :ref:`tessere` personali iniziali

3. si crea il primo turno: se il torneo è associato a una particolare :ref:`valutazione Glicko
   <gestione valutazioni glicko>` verrà generato utilizzando la valutazione corrente di ciascun
   giocatore, altrimenti in ordine casuale; in ogni caso il gestore del torneo può aggiustare a
   mano le combinazione generate dal computer

4. si stampano i :ref:`cartellini` per il nuovo turno

5. si gioca!

6. si raccolgono i :ref:`cartellini` compilati e si inseriscono i risultati

7. eventualmente si stampa la classifica aggiornata

8. si da ascolto ai pignoli che fanno notare qualche errore, si corregge e si ristampa

9. nel caso, si permette l'iscrizione di ulteriori giocatori o si consente il ritiro a chi lo
   richiede

10. si genera il prossimo turno e si ripete dal punto 4 in poi fino a piena soddisfazione,
    oppure finché sono possibili ulteriori combinazioni

11. se si desidera, si gioca il :ref:`turno finale <turno finale>` per stabilire le prime
    quattro posizioni nella classifica

12. si assegnano i premi finali, si stampa la classifica e le :ref:`tessere` finali

__ http://it.wikipedia.org/wiki/Fantasma_Formaggino

.. toctree::
   :maxdepth: 2

   autenticazione
   anagrafiche
   altre
