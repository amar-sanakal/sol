.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   mar 11 nov 2008 23:12:11 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2009, 2010, 2014, 2016 Lele Gaifax
..

==========
 Stampati
==========

.. hint:: A partire dalla versione 3.33, la maggior parte delle stampe mostra un QRCode__ sulla
          prima pagina che contiene l'hyperlink alla corrispondente `pagina Lit`. Il QRCode può
          essere letto su uno smartphone con una delle svariate applicazioni gratuite
          disponibili, piuttosto che semplicemente cliccato quando il PDF viene visualizzato su
          un computer tradizionale.

          Il QRCode **non appare** quando viene usato l'indirizzo ``localhost`` per accedere
          all'istanza locale di SoL. Per farlo apparire, devi usare l'indirizzo IP del
          computer: quando possibile, SoL lo mostra alla partenza::

            Your IP address seems to be 192.168.1.100
            Starting server in PID 8131.
            serving on http://0.0.0.0:6996

          Invece che usare l'URL ``http://localhost:6996/`` per accedere a SoL, utilizza
          ``http://192.168.1.100:6996/`` e il QRCode dovrebbe apparire ed essere utilizzabile
          da altri dispositivi connessi alla tua rete locale.

__ https://it.wikipedia.org/wiki/Codice_QR

.. _concorrenti:

Concorrenti
===========

.. figure:: concorrenti.png


.. _tessere:

Tessere
=======

.. figure:: tesserefinali.png


.. _cartellini:

Cartellini
==========

.. figure:: cartellinifinali.png


.. _risultati:

Risultati
=========

.. figure:: risultati.png


.. _classifica:

Classifica
==========

.. figure:: classifica.png
