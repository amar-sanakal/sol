.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 03 mar 2014 12:42:56 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

=============================================
 :mod:`sol.models.competitor` -- Competitors
=============================================

.. automodule:: sol.models.competitor

.. autoclass:: Competitor
   :members:
