.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   sab 08 nov 2008 20:43:11 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2008, 2010, 2013, 2014 Lele Gaifax
..

=============
 SoL modules
=============

.. toctree::
   :maxdepth: 2

   i18n
   models
   printouts
   views
