championships:
- {closed: false, description: Tournoi De Pole, year: 2014}
clubs:
- {couplings: serial, description: Paris WSD}
- {couplings: serial, description: Villepinte}
- {couplings: serial, description: "Le M\xE9e sur Seine"}
players:
- {club: 1, firstname: Thierry, lastname: Huchet, nationality: FRA, sex: M}
- {club: 1, firstname: Thibault, lastname: Colin, nationality: FRA, sex: M}
- {club: 2, firstname: Mouraly, lastname: Venou, nationality: FRA, sex: M}
- {club: 2, firstname: Kandasamy, lastname: Nandakumar, nationality: FRA, sex: M}
- {club: 3, firstname: Yoann, lastname: Pidial, nationality: FRA, sex: M}
- {club: 3, firstname: Laetitia, lastname: Pidial, nationality: FRA, sex: F}
- {club: 3, firstname: Chandrakant, lastname: Pidial, nationality: FRA, sex: M}
- {club: 3, firstname: Thierry, lastname: "Berth\xE9l\xE9my", nationality: FRA, sex: M}
- {club: 2, firstname: Alfred, lastname: Rajadurai, nationality: FRA, sex: M}
- {club: 2, firstname: Renato, lastname: Felizardo, nationality: FRA, sex: M}
- {club: 2, firstname: Fabian, lastname: Pereira, nationality: FRA, sex: M}
- {club: 3, firstname: Sattirabady, lastname: Jupiter, nationality: FRA, sex: M}
- {firstname: "Beno\xEEt", lastname: Lamy, nationality: FRA, sex: M}
- {club: 2, firstname: Yuvaradj, lastname: Ducher, nationality: FRA, sex: M}
- {firstname: Olivier, lastname: Telias, nationality: FRA, sex: M}
- {club: 3, firstname: Christian, lastname: Cadet}
- {firstname: Dinesh, lastname: Bascarane}
- {firstname: Norbert, lastname: Mothe}
- {firstname: Francissco, lastname: Fernandez}
- {firstname: Sieger, lastname: Kevin, sex: M}
- {firstname: Gnanadicom, lastname: Christy, sex: M}
- {firstname: Fourcade, lastname: Cedric, sex: M}
- {firstname: Dubus, lastname: Roland}
- {firstname: Rayar, lastname: Gerard}
- {firstname: Alwis, lastname: Sudath, sex: M}
- {firstname: Soudidier, lastname: Olivier}
- {firstname: Lusardi, lastname: "Fr\xE9d\xE9ric", sex: M}
- {firstname: Tworek, lastname: "Fr\xE9d\xE9ric", sex: M}
- {firstname: Hasrouni, lastname: Samy}
seasons:
- {closed: false, club: 1, description: '2016', playersperteam: 1, prizefactor: '0.00',
  prizes: fixed, skipworstprizes: 0}
---
championship: 1
competitors:
- bucholz: 41
  netscore: 11
  players: [1]
  points: 6
  totscore: 98
- bucholz: 32
  netscore: 42
  players: [2]
  points: 8
  totscore: 106
- bucholz: 52
  netscore: 4
  players: [3]
  points: 8
  totscore: 86
- bucholz: 31
  netscore: -62
  players: [4]
  points: 4
  totscore: 49
- bucholz: 34
  netscore: 46
  players: [5]
  points: 8
  totscore: 103
- bucholz: 25
  netscore: -34
  players: [6]
  points: 4
  totscore: 58
- bucholz: 34
  netscore: -101
  players: [7]
  points: 2
  totscore: 21
- bucholz: 31
  netscore: 5
  players: [8]
  points: 6
  totscore: 74
- bucholz: 36
  netscore: 46
  players: [9]
  points: 8
  totscore: 107
- bucholz: 37
  netscore: 11
  players: [10]
  points: 6
  totscore: 91
- bucholz: 42
  netscore: 127
  players: [11]
  points: 12
  totscore: 150
- bucholz: 37
  netscore: -15
  players: [12]
  points: 6
  totscore: 68
- bucholz: 44
  netscore: 48
  players: [13]
  points: 10
  totscore: 116
- bucholz: 40
  netscore: 75
  players: [14]
  points: 8
  totscore: 130
- bucholz: 37
  netscore: 45
  players: [15]
  points: 9
  totscore: 123
- bucholz: 30
  netscore: -41
  players: [16]
  points: 4
  totscore: 55
- bucholz: 41
  netscore: -32
  players: [17]
  points: 4
  totscore: 63
- bucholz: 38
  netscore: 24
  players: [18]
  points: 6
  totscore: 100
- bucholz: 39
  netscore: 80
  players: [19]
  points: 8
  totscore: 124
- bucholz: 48
  netscore: 29
  players: [20]
  points: 8
  totscore: 122
- bucholz: 27
  netscore: -64
  players: [21]
  points: 3
  totscore: 39
- bucholz: 36
  netscore: -1
  players: [22]
  points: 6
  totscore: 70
- bucholz: 26
  netscore: -57
  players: [23]
  points: 4
  totscore: 45
- bucholz: 40
  netscore: -9
  players: [24]
  points: 6
  totscore: 82
- bucholz: 45
  netscore: -21
  players: [25]
  points: 5
  totscore: 85
- bucholz: 32
  netscore: 33
  players: [26]
  points: 6
  totscore: 89
- bucholz: 36
  netscore: -29
  players: [27]
  points: 4
  totscore: 62
- bucholz: 40
  netscore: -15
  players: [28]
  points: 6
  totscore: 75
- bucholz: 21
  netscore: 5
  players: [29]
  points: 5
  totscore: 70
couplings: serial
currentturn: 6
date: 2016-03-19
description: Tournoi Depole
duration: 45
location: Paris
matches:
- {board: 1, competitor1: 4, competitor2: 27, score1: 0, score2: 25, turn: 1}
- {board: 2, competitor1: 28, competitor2: 23, score1: 25, score2: 0, turn: 1}
- {board: 3, competitor1: 3, competitor2: 14, score1: 25, score2: 11, turn: 1}
- {board: 4, competitor1: 10, competitor2: 9, score1: 0, score2: 25, turn: 1}
- {board: 5, competitor1: 5, competitor2: 25, score1: 10, score2: 16, turn: 1}
- {board: 6, competitor1: 17, competitor2: 29, score1: 17, score2: 6, turn: 1}
- {board: 7, competitor1: 20, competitor2: 2, score1: 25, score2: 6, turn: 1}
- {board: 8, competitor1: 6, competitor2: 19, score1: 0, score2: 25, turn: 1}
- {board: 9, competitor1: 12, competitor2: 11, score1: 0, score2: 25, turn: 1}
- {board: 10, competitor1: 1, competitor2: 7, score1: 25, score2: 0, turn: 1}
- {board: 11, competitor1: 22, competitor2: 24, score1: 2, score2: 25, turn: 1}
- {board: 12, competitor1: 18, competitor2: 21, score1: 25, score2: 0, turn: 1}
- {board: 13, competitor1: 8, competitor2: 26, score1: 0, score2: 24, turn: 1}
- {board: 14, competitor1: 15, competitor2: 13, score1: 15, score2: 23, turn: 1}
- {board: 15, competitor1: 16, competitor2: null, score1: 25, score2: 0, turn: 1}
- {board: 1, competitor1: 1, competitor2: 9, score1: 17, score2: 12, turn: 2}
- {board: 2, competitor1: 11, competitor2: 16, score1: 25, score2: 0, turn: 2}
- {board: 3, competitor1: 18, competitor2: 19, score1: 17, score2: 16, turn: 2}
- {board: 4, competitor1: 27, competitor2: 28, score1: 7, score2: 25, turn: 2}
- {board: 5, competitor1: 26, competitor2: 24, score1: 9, score2: 14, turn: 2}
- {board: 6, competitor1: 20, competitor2: 3, score1: 13, score2: 17, turn: 2}
- {board: 7, competitor1: 17, competitor2: 13, score1: 4, score2: 25, turn: 2}
- {board: 8, competitor1: 25, competitor2: 15, score1: 20, score2: 20, turn: 2}
- {board: 9, competitor1: 5, competitor2: 29, score1: 25, score2: 0, turn: 2}
- {board: 10, competitor1: 14, competitor2: 2, score1: 25, score2: 0, turn: 2}
- {board: 11, competitor1: 4, competitor2: 6, score1: 17, score2: 5, turn: 2}
- {board: 12, competitor1: 22, competitor2: 8, score1: 16, score2: 1, turn: 2}
- {board: 13, competitor1: 7, competitor2: 10, score1: 0, score2: 25, turn: 2}
- {board: 14, competitor1: 12, competitor2: 21, score1: 25, score2: 0, turn: 2}
- {board: 15, competitor1: 23, competitor2: null, score1: 25, score2: 0, turn: 2}
- {board: 1, competitor1: 11, competitor2: 28, score1: 25, score2: 0, turn: 3}
- {board: 2, competitor1: 24, competitor2: 3, score1: 0, score2: 25, turn: 3}
- {board: 3, competitor1: 13, competitor2: 1, score1: 18, score2: 13, turn: 3}
- {board: 4, competitor1: 18, competitor2: 25, score1: 12, score2: 20, turn: 3}
- {board: 5, competitor1: 9, competitor2: 27, score1: 25, score2: 1, turn: 3}
- {board: 6, competitor1: 19, competitor2: 26, score1: 25, score2: 0, turn: 3}
- {board: 7, competitor1: 12, competitor2: 16, score1: 25, score2: 1, turn: 3}
- {board: 8, competitor1: 20, competitor2: 14, score1: 24, score2: 19, turn: 3}
- {board: 9, competitor1: 23, competitor2: 22, score1: 4, score2: 25, turn: 3}
- {board: 10, competitor1: 17, competitor2: 5, score1: 12, score2: 20, turn: 3}
- {board: 11, competitor1: 15, competitor2: 7, score1: 25, score2: 0, turn: 3}
- {board: 12, competitor1: 10, competitor2: 4, score1: 25, score2: 7, turn: 3}
- {board: 13, competitor1: 21, competitor2: 29, score1: 7, score2: 7, turn: 3}
- {board: 14, competitor1: 6, competitor2: 8, score1: 3, score2: 19, turn: 3}
- {board: 15, competitor1: 2, competitor2: null, score1: 25, score2: 0, turn: 3}
- {board: 1, competitor1: 13, competitor2: 25, score1: 25, score2: 10, turn: 4}
- {board: 2, competitor1: 1, competitor2: 20, score1: 14, score2: 21, turn: 4}
- {board: 3, competitor1: 24, competitor2: 9, score1: 24, score2: 25, turn: 4}
- {board: 4, competitor1: 11, competitor2: 3, score1: 25, score2: 8, turn: 4}
- {board: 5, competitor1: 18, competitor2: 28, score1: 25, score2: 4, turn: 4}
- {board: 6, competitor1: 12, competitor2: 5, score1: 0, score2: 25, turn: 4}
- {board: 7, competitor1: 22, competitor2: 19, score1: 0, score2: 25, turn: 4}
- {board: 8, competitor1: 26, competitor2: 27, score1: 6, score2: 17, turn: 4}
- {board: 9, competitor1: 10, competitor2: 15, score1: 16, score2: 23, turn: 4}
- {board: 10, competitor1: 16, competitor2: 23, score1: 8, score2: 16, turn: 4}
- {board: 11, competitor1: 14, competitor2: 17, score1: 25, score2: 0, turn: 4}
- {board: 12, competitor1: 2, competitor2: 8, score1: 25, score2: 14, turn: 4}
- {board: 13, competitor1: 6, competitor2: null, score1: 25, score2: 0, turn: 4}
- {board: 14, competitor1: 29, competitor2: 7, score1: 8, score2: 9, turn: 4}
- {board: 15, competitor1: 4, competitor2: 21, score1: 25, score2: 6, turn: 4}
- {board: 1, competitor1: 20, competitor2: 18, score1: 24, score2: 12, turn: 5}
- {board: 2, competitor1: 19, competitor2: 5, score1: 19, score2: 12, turn: 5}
- {board: 3, competitor1: 15, competitor2: 28, score1: 25, score2: 5, turn: 5}
- {board: 4, competitor1: 14, competitor2: 24, score1: 25, score2: 0, turn: 5}
- {board: 5, competitor1: 11, competitor2: 13, score1: 25, score2: 0, turn: 5}
- {board: 6, competitor1: 25, competitor2: 1, score1: 11, score2: 23, turn: 5}
- {board: 7, competitor1: 3, competitor2: 9, score1: 10, score2: 8, turn: 5}
- {board: 8, competitor1: 10, competitor2: 12, score1: 25, score2: 0, turn: 5}
- {board: 9, competitor1: 2, competitor2: 4, score1: 25, score2: 0, turn: 5}
- {board: 10, competitor1: 27, competitor2: 22, score1: 5, score2: 17, turn: 5}
- {board: 11, competitor1: 26, competitor2: 16, score1: 25, score2: 0, turn: 5}
- {board: 12, competitor1: 23, competitor2: 17, score1: 0, score2: 25, turn: 5}
- {board: 13, competitor1: 8, competitor2: 21, score1: 21, score2: 1, turn: 5}
- {board: 14, competitor1: 29, competitor2: null, score1: 25, score2: 0, turn: 5}
- {board: 15, competitor1: 7, competitor2: 6, score1: 7, score2: 18, turn: 5}
- {board: 1, competitor1: 19, competitor2: 15, score1: 14, score2: 15, turn: 6}
- {board: 2, competitor1: 9, competitor2: 18, score1: 12, score2: 9, turn: 6}
- {board: 3, competitor1: 11, competitor2: 20, score1: 25, score2: 15, turn: 6}
- {board: 4, competitor1: 5, competitor2: 22, score1: 11, score2: 10, turn: 6}
- {board: 5, competitor1: 14, competitor2: 1, score1: 25, score2: 6, turn: 6}
- {board: 6, competitor1: 10, competitor2: 2, score1: 0, score2: 25, turn: 6}
- {board: 7, competitor1: 25, competitor2: 28, score1: 8, score2: 16, turn: 6}
- {board: 8, competitor1: 3, competitor2: 13, score1: 1, score2: 25, turn: 6}
- {board: 9, competitor1: 24, competitor2: 17, score1: 19, score2: 5, turn: 6}
- {board: 10, competitor1: 26, competitor2: 4, score1: 25, score2: 0, turn: 6}
- {board: 11, competitor1: 12, competitor2: 27, score1: 18, score2: 7, turn: 6}
- {board: 12, competitor1: 8, competitor2: 23, score1: 19, score2: 0, turn: 6}
- {board: 13, competitor1: 6, competitor2: 29, score1: 7, score2: 24, turn: 6}
- {board: 14, competitor1: 7, competitor2: 16, score1: 5, score2: 21, turn: 6}
- {board: 15, competitor1: 21, competitor2: null, score1: 25, score2: 0, turn: 6}
prealarm: 5
prized: false
prizefactor: '1.00'
prizes: millesimal
rankedturn: 6
season: 1
