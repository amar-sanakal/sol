championships:
- {closed: false, club: 1, couplings: serial, description: Campionato CCM 2014-2015,
  guid: 80e7e26c82cd11e4a2c783be8c3acaa2, modified: !!timestamp '2014-12-13 13:39:47',
  owner: 1, playersperteam: 1, previous: 2, prizes: fixed40, skipworstprizes: 0}
- {closed: true, club: 1, couplings: serial, description: Campionato CCM 2013-2014,
  guid: 38449cf682cd11e4a18183be8c3acaa2, modified: !!timestamp '2014-12-13 13:38:45',
  owner: 1, playersperteam: 1, previous: 3, prizes: fixed40, skipworstprizes: 0}
- {closed: false, club: 1, couplings: serial, description: Campionato CCM 2012-2013,
  guid: 9f35bee6841711e3ac012e92809967e5, modified: !!timestamp '2014-01-23 10:17:54',
  playersperteam: 1, previous: 4, prizes: millesimal, skipworstprizes: 0}
- {closed: true, club: 1, couplings: serial, description: Campionato CCM 2010-2011,
  guid: 9f29519c841711e3ac012e92809967e5, modified: !!timestamp '2014-01-23 10:17:54',
  playersperteam: 1, previous: 5, prizes: millesimal, skipworstprizes: 2}
- {closed: true, club: 1, couplings: serial, description: Campionato CCM 2009-2010,
  guid: 9f2530ee841711e3ac012e92809967e5, modified: !!timestamp '2014-01-23 10:17:54',
  playersperteam: 1, previous: 6, prizes: millesimal, skipworstprizes: 2}
- {closed: true, club: 1, couplings: serial, description: Campionato CCM 2008-2009,
  guid: 9f1c6810841711e3ac012e92809967e5, modified: !!timestamp '2014-01-23 10:17:53',
  playersperteam: 1, prizes: millesimal, skipworstprizes: 1}
clubs:
- {couplings: serial, description: Carrom Club Milano, emblem: ccm.png, guid: 9d70a7ba841711e3ac012e92809967e5,
  modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA, prizes: fixed, siteurl: 'http://www.carromclubmilano.it/'}
- {couplings: serial, description: Carrom Club Verona, email: dafattid@cpne.it, guid: 9d72482c841711e3ac012e92809967e5,
  modified: !!timestamp '2014-07-15 11:32:57', nationality: ITA, owner: 1, prizes: fixed}
- {couplings: dazed, description: Italian Carrom Federation, email: info@carromitaly.com,
  emblem: fic.png, guid: 9d70ecb6841711e3ac012e92809967e5, isfederation: true, modified: !!timestamp '2014-01-23
    12:23:02', nationality: ITA, prizes: millesimal, siteurl: 'http://www.carromitaly.com/'}
- {couplings: dazed, description: Carrom Club Guastalla, email: truffelli.guido@gmail.com,
  emblem: e1631bbb5615594ac38b88418ab607a2.png, guid: 9d74241c841711e3ac012e92809967e5,
  modified: !!timestamp '2014-07-14 12:24:18', nationality: ITA, owner: 4, prizes: asis}
- {couplings: serial, description: Ceylaniens Carrom Club, guid: 9d71355e841711e3ac012e92809967e5,
  modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA, prizes: fixed}
players:
- birthdate: 1964-02-21
  citizenship: true
  club: 2
  email: dafattid@cpne.it
  federation: 3
  firstname: Daniele
  guid: 9df88f54841711e3ac012e92809967e5
  language: it
  lastname: Da Fatti
  merged:
  - [596799729e7811e3ac012e92809967e5, '', '', '']
  - [8edf915ea08f11e3ac012e92809967e5, Dafatti, Daniele, '']
  modified: 2014-08-28 13:56:10
  nationality: ITA
  nickname: dafattid
  owner: 1
  phone: '3485813884'
  sex: M
- {citizenship: false, club: 1, federation: 3, firstname: Amila, guid: 9df37b5e841711e3ac012e92809967e5,
  lastname: Amarasingha, modified: !!timestamp '2014-01-25 11:21:20', nationality: ITA}
- {citizenship: true, club: 4, firstname: Simone, guid: 9e584f2a841711e3ac012e92809967e5,
  lastname: Beltrami, modified: !!timestamp '2014-07-14 07:18:29', nationality: ITA,
  nickname: Mario, owner: 4, sex: M}
- {birthdate: 1966-09-17, citizenship: true, club: 4, email: truffelli.guido@gmail.com,
  federation: 3, firstname: Guido, guid: 9e05eb54841711e3ac012e92809967e5, language: it,
  lastname: Truffelli, modified: !!timestamp '2014-07-14 07:05:12', nationality: ITA,
  nickname: Psy, owner: 4, portrait: ef64e38174f1d96bc7ce7c0a9e5adadb.jpeg, sex: M}
- {citizenship: true, club: 1, firstname: Vittorio, guid: 9d83946a841711e3ac012e92809967e5,
  lastname: Canisi, modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA,
  portrait: vittorio.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Stefano, guid: 9d86879c841711e3ac012e92809967e5,
  lastname: Fabiano, modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA,
  nickname: stefano, sex: M}
- {citizenship: false, club: 2, federation: 3, firstname: Sasi, guid: 9e814df8841711e3ac012e92809967e5,
  lastname: Fernando, modified: !!timestamp '2014-01-25 11:41:41', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: false, club: 2, firstname: Suresh, guid: 9e8e9116841711e3ac012e92809967e5,
  lastname: Fernando, modified: !!timestamp '2014-08-28 14:08:06', nationality: ITA,
  owner: 1, sex: M}
- {citizenship: true, firstname: Dineth, guid: 172210c4ebfd11e3ac012e92809967e5, lastname: Hapuarachchige,
  modified: !!timestamp '2014-06-04 15:29:59'}
- {citizenship: true, club: 1, firstname: Riccardo, guid: 9d871ee6841711e3ac012e92809967e5,
  lastname: Maurizi, modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA,
  portrait: ricki.jpg}
- {citizenship: true, club: 1, firstname: Luca, guid: 9d9c985c841711e3ac012e92809967e5,
  lastname: Ometto, modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA}
- {citizenship: true, club: 1, firstname: Paolo, guid: 9d826324841711e3ac012e92809967e5,
  lastname: Ometto, modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA,
  portrait: paolo.jpg, sex: M}
- {citizenship: true, club: 4, firstname: Marco, guid: 9e6028c6841711e3ac012e92809967e5,
  lastname: Simonazzi, modified: !!timestamp '2014-07-14 17:54:10', nationality: ITA,
  owner: 4, portrait: 278a67c4304739b773bbee6ef5f722bf.jpeg, sex: M}
- {citizenship: true, club: 3, firstname: Stefano, guid: 9d809abc841711e3ac012e92809967e5,
  lastname: Stucchi, modified: !!timestamp '2014-01-23 10:17:51', nationality: ITA,
  portrait: Stucchi.jpg, sex: M}
- {citizenship: false, club: 1, federation: 3, firstname: Amila, guid: 9d915636841711e3ac012e92809967e5,
  lastname: Ukwatha, modified: !!timestamp '2014-01-23 22:42:02', nationality: ITA}
- citizenship: true
  club: 2
  federation: 3
  firstname: Ayesh Nilan
  guid: 9eab8f32841711e3ac012e92809967e5
  lastname: Vanderlan
  merged:
  - [9e91a374841711e3ac012e92809967e5, '', '', '']
  - [c6081e6af34811e3ac012e92809967e5, Vanderlan, Ayesh, '']
  modified: 2014-08-28 14:10:12
  nationality: ITA
  owner: 1
  sex: M
- {citizenship: true, club: 5, firstname: Lakidu, guid: 9d88df1a841711e3ac012e92809967e5,
  lastname: Vidana, modified: !!timestamp '2014-01-23 22:37:41', nationality: ITA,
  portrait: lakidu.jpg, sex: M}
rates: []
ratings: []
---
championship: 1
competitors:
- bucholz: 59
  netscore: -15
  players: [2]
  points: 6
  totscore: 108
- bucholz: 40
  netscore: -101
  players: [3]
  points: 2
  totscore: 35
- bucholz: 53
  netscore: 36
  players: [5]
  points: 8
  totscore: 117
- bucholz: 56
  netscore: -53
  players: [1]
  points: 6
  totscore: 72
- bucholz: 43
  netscore: 10
  players: [6]
  points: 7
  totscore: 90
- bucholz: 42
  netscore: 35
  players: [7]
  points: 8
  totscore: 136
- bucholz: 63
  netscore: -6
  players: [8]
  points: 10
  totscore: 97
- bucholz: 49
  netscore: 69
  players: [9]
  points: 10
  totscore: 138
- bucholz: 51
  netscore: 42
  players: [10]
  points: 8
  totscore: 130
- bucholz: 45
  netscore: -69
  players: [11]
  points: 4
  totscore: 61
- bucholz: 53
  netscore: 61
  players: [12]
  points: 10
  totscore: 122
- bucholz: 37
  netscore: -60
  players: [13]
  points: 4
  totscore: 48
- bucholz: 55
  netscore: 54
  players: [14]
  points: 10
  totscore: 133
- bucholz: 42
  netscore: -15
  players: [4]
  points: 6
  totscore: 84
- bucholz: 31
  netscore: 70
  players: [15]
  points: 8
  totscore: 131
- bucholz: 63
  netscore: 84
  players: [16]
  points: 10
  totscore: 135
- bucholz: 61
  netscore: 33
  players: [17]
  points: 9
  totscore: 133
couplings: serial
currentturn: 7
date: 2014-12-14
delaytoppairing: 1
description: Open Milano 2014-2015
duration: 45
finalkind: simple
finals: 1
guid: c22763f682ce11e48f9983be8c3acaa2
location: Piazzale Perrucchetti
matches:
- {board: 1, competitor1: 3, competitor2: 2, final: false, score1: 25, score2: 0,
  turn: 1}
- {board: 2, competitor1: 12, competitor2: 5, final: false, score1: 0, score2: 19,
  turn: 1}
- {board: 3, competitor1: 9, competitor2: 6, final: false, score1: 21, score2: 13,
  turn: 1}
- {board: 4, competitor1: 10, competitor2: 13, final: false, score1: 0, score2: 25,
  turn: 1}
- {board: 5, competitor1: 11, competitor2: 1, final: false, score1: 16, score2: 9,
  turn: 1}
- {board: 6, competitor1: 7, competitor2: 4, final: false, score1: 21, score2: 8,
  turn: 1}
- {board: 7, competitor1: 16, competitor2: 8, final: false, score1: 25, score2: 0,
  turn: 1}
- {board: 8, competitor1: 17, competitor2: 15, final: false, score1: 25, score2: 8,
  turn: 1}
- {board: 9, competitor1: 14, competitor2: null, final: false, score1: 25, score2: 0,
  turn: 1}
- {board: 1, competitor1: 14, competitor2: 16, final: false, score1: 0, score2: 25,
  turn: 2}
- {board: 2, competitor1: 3, competitor2: 13, final: false, score1: 10, score2: 19,
  turn: 2}
- {board: 3, competitor1: 5, competitor2: 17, final: false, score1: 10, score2: 10,
  turn: 2}
- {board: 4, competitor1: 7, competitor2: 9, final: false, score1: 11, score2: 9,
  turn: 2}
- {board: 5, competitor1: 15, competitor2: 12, final: false, score1: 23, score2: 0,
  turn: 2}
- {board: 6, competitor1: 11, competitor2: 6, final: false, score1: 7, score2: 25,
  turn: 2}
- {board: 7, competitor1: 1, competitor2: 4, final: false, score1: 25, score2: 6,
  turn: 2}
- {board: 8, competitor1: 2, competitor2: 8, final: false, score1: 0, score2: 25,
  turn: 2}
- {board: 9, competitor1: 10, competitor2: null, final: false, score1: 25, score2: 0,
  turn: 2}
- {board: 1, competitor1: 7, competitor2: 17, final: false, score1: 24, score2: 11,
  turn: 3}
- {board: 2, competitor1: 8, competitor2: 10, final: false, score1: 25, score2: 0,
  turn: 3}
- {board: 3, competitor1: 16, competitor2: 13, final: false, score1: 21, score2: 3,
  turn: 3}
- {board: 4, competitor1: 3, competitor2: 6, final: false, score1: 25, score2: 4,
  turn: 3}
- {board: 5, competitor1: 5, competitor2: 9, final: false, score1: 4, score2: 25,
  turn: 3}
- {board: 6, competitor1: 15, competitor2: 1, final: false, score1: 15, score2: 25,
  turn: 3}
- {board: 7, competitor1: 14, competitor2: 11, final: false, score1: 0, score2: 25,
  turn: 3}
- {board: 8, competitor1: 4, competitor2: 12, final: false, score1: 25, score2: 0,
  turn: 3}
- {board: 9, competitor1: 2, competitor2: null, final: false, score1: 25, score2: 0,
  turn: 3}
- {board: 1, competitor1: 13, competitor2: 9, final: false, score1: 22, score2: 14,
  turn: 4}
- {board: 2, competitor1: 16, competitor2: 7, final: false, score1: 25, score2: 0,
  turn: 4}
- {board: 3, competitor1: 8, competitor2: 3, final: false, score1: 21, score2: 15,
  turn: 4}
- {board: 4, competitor1: 1, competitor2: 17, final: false, score1: 7, score2: 25,
  turn: 4}
- {board: 5, competitor1: 6, competitor2: 4, final: false, score1: 19, score2: 22,
  turn: 4}
- {board: 6, competitor1: 14, competitor2: 2, final: false, score1: 25, score2: 1,
  turn: 4}
- {board: 7, competitor1: 10, competitor2: 15, final: false, score1: 0, score2: 25,
  turn: 4}
- {board: 8, competitor1: 11, competitor2: 5, final: false, score1: 16, score2: 7,
  turn: 4}
- {board: 9, competitor1: 12, competitor2: null, final: false, score1: 25, score2: 0,
  turn: 4}
- {board: 1, competitor1: 4, competitor2: 15, final: false, score1: 11, score2: 10,
  turn: 5}
- {board: 2, competitor1: 1, competitor2: 9, final: false, score1: 21, score2: 11,
  turn: 5}
- {board: 3, competitor1: 5, competitor2: 10, final: false, score1: 25, score2: 4,
  turn: 5}
- {board: 4, competitor1: 16, competitor2: 11, final: false, score1: 5, score2: 22,
  turn: 5}
- {board: 5, competitor1: 7, competitor2: 13, final: false, score1: 19, score2: 14,
  turn: 5}
- {board: 6, competitor1: 8, competitor2: 17, final: false, score1: 17, score2: 22,
  turn: 5}
- {board: 7, competitor1: 6, competitor2: null, final: false, score1: 25, score2: 0,
  turn: 5}
- {board: 8, competitor1: 3, competitor2: 14, final: false, score1: 16, score2: 12,
  turn: 5}
- {board: 9, competitor1: 2, competitor2: 12, final: false, score1: 4, score2: 11,
  turn: 5}
- {board: 1, competitor1: 9, competitor2: 14, final: false, score1: 25, score2: 10,
  turn: 6}
- {board: 2, competitor1: 15, competitor2: null, final: false, score1: 25, score2: 0,
  turn: 6}
- {board: 3, competitor1: 7, competitor2: 11, final: false, score1: 15, score2: 11,
  turn: 6}
- {board: 4, competitor1: 4, competitor2: 8, final: false, score1: 0, score2: 25,
  turn: 6}
- {board: 5, competitor1: 16, competitor2: 17, final: false, score1: 9, score2: 25,
  turn: 6}
- {board: 6, competitor1: 3, competitor2: 5, final: false, score1: 25, score2: 0,
  turn: 6}
- {board: 7, competitor1: 6, competitor2: 12, final: false, score1: 25, score2: 5,
  turn: 6}
- {board: 8, competitor1: 1, competitor2: 13, final: false, score1: 0, score2: 25,
  turn: 6}
- {board: 9, competitor1: 10, competitor2: 2, final: false, score1: 25, score2: 5,
  turn: 6}
- {board: 1, competitor1: 16, competitor2: 3, final: false, score1: 25, score2: 1,
  turn: 7}
- {board: 2, competitor1: 11, competitor2: 4, final: false, score1: 25, score2: 0,
  turn: 7}
- {board: 3, competitor1: 1, competitor2: 6, final: false, score1: 21, score2: 25,
  turn: 7}
- {board: 4, competitor1: 15, competitor2: 2, final: false, score1: 25, score2: 0,
  turn: 7}
- {board: 5, competitor1: 5, competitor2: null, final: false, score1: 25, score2: 0,
  turn: 7}
- {board: 6, competitor1: 9, competitor2: 10, final: false, score1: 25, score2: 7,
  turn: 7}
- {board: 7, competitor1: 17, competitor2: 13, final: false, score1: 15, score2: 25,
  turn: 7}
- {board: 8, competitor1: 14, competitor2: 12, final: false, score1: 12, score2: 7,
  turn: 7}
- {board: 9, competitor1: 7, competitor2: 8, final: false, score1: 7, score2: 25,
  turn: 7}
modified: 2014-12-18 10:48:00
owner: 1
phantomscore: 25
prealarm: 5
prized: false
rankedturn: 7
