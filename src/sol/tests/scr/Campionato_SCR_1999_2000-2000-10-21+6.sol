championships: []
clubs:
- {couplings: dazed, description: Scarambol Club Rovereto, email: prova@metapensiero.it,
  emblem: scr.png, guid: 7cf30e3c6bb811e3baa63085a99ccac7, modified: !!timestamp '2013-12-23
    10:03:22', nationality: ITA, prizes: fixed, siteurl: 'http://scr.artiemestieri.tn.it/'}
players:
- {citizenship: true, club: 1, firstname: Paolo, guid: 7cf871886bb811e3baa63085a99ccac7,
  lastname: Cominelli, modified: !!timestamp '2013-12-23 09:56:26', nationality: ITA,
  portrait: cominelli.jpg}
- {citizenship: true, club: 1, firstname: Emanuele, guid: 7cf905806bb811e3baa63085a99ccac7,
  lastname: Gaifas, modified: !!timestamp '2013-12-23 09:56:26', nationality: ITA,
  nickname: lele, portrait: egaifas.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Juri, guid: 7cff74f66bb811e3baa63085a99ccac7,
  lastname: Golin, modified: !!timestamp '2013-12-23 09:56:26', nationality: ITA,
  nickname: Picol, portrait: juri.jpg}
- {citizenship: true, club: 1, firstname: Maurizio, guid: 7d012b986bb811e3baa63085a99ccac7,
  lastname: Sassosi, modified: !!timestamp '2013-12-23 09:56:26', nationality: ITA,
  nickname: Bob, portrait: msassosi.jpg}
- {citizenship: true, club: 1, firstname: Sandro, guid: 7d037e3e6bb811e3baa63085a99ccac7,
  lastname: Sartori, modified: !!timestamp '2013-12-23 09:56:26', nationality: ITA,
  portrait: ssartori.jpg}
- {citizenship: true, club: 1, firstname: Stefania, guid: 7d0401746bb811e3baa63085a99ccac7,
  lastname: Andreolli, modified: !!timestamp '2013-12-23 09:56:26', nationality: ITA,
  portrait: stef.jpg, sex: F}
- {citizenship: true, club: 1, firstname: Roberto, guid: 7d3904006bb811e3baa63085a99ccac7,
  lastname: Potrich, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  nickname: Blond, portrait: blond.jpg}
- {citizenship: true, club: 1, firstname: Fausto, guid: 7d5957966bb811e3baa63085a99ccac7,
  lastname: Gaifas, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  portrait: Fausto.jpg}
- {citizenship: true, club: 1, firstname: Stefano, guid: 7d59ea766bb811e3baa63085a99ccac7,
  lastname: Consolati, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA}
- {citizenship: true, club: 1, firstname: Letizia, guid: 7d5c45466bb811e3baa63085a99ccac7,
  lastname: Pezzato, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  portrait: lpezzato.jpg, sex: F}
- {citizenship: true, club: 1, firstname: Francesco, guid: 7d5cd63c6bb811e3baa63085a99ccac7,
  lastname: Ingrosso, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  portrait: fingrosso.jpg}
- {citizenship: true, club: 1, firstname: Cristina, guid: 7d5d68906bb811e3baa63085a99ccac7,
  lastname: Candioli, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  sex: F}
- {citizenship: true, club: 1, firstname: Paolo, guid: 7d5df9ae6bb811e3baa63085a99ccac7,
  lastname: Lorenzi, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  portrait: plorenzi.jpg}
- {citizenship: true, club: 1, firstname: Massimo, guid: 7d5e91de6bb811e3baa63085a99ccac7,
  lastname: Dalus, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA}
- {citizenship: true, club: 1, firstname: Renato, guid: 7d60f7c66bb811e3baa63085a99ccac7,
  lastname: Simoncelli, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  nickname: Over, portrait: over.jpg}
- {citizenship: true, club: 1, firstname: Giuliano, guid: 7d617fac6bb811e3baa63085a99ccac7,
  lastname: Badanai, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA}
- {citizenship: true, firstname: Paolo, guid: 7d620f086bb811e3baa63085a99ccac7, lastname: Calliari,
  modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA}
- {citizenship: true, firstname: Pecu, guid: 7d6293426bb811e3baa63085a99ccac7, lastname: Pecu,
  modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA}
- {citizenship: true, firstname: Andrea, guid: 7d6315d86bb811e3baa63085a99ccac7, lastname: Manfredi,
  modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA}
- {citizenship: true, club: 1, firstname: Enrico, guid: 7d65b1626bb811e3baa63085a99ccac7,
  lastname: Peroni, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  nickname: Pea, portrait: eperoni.jpg}
- {citizenship: true, club: 1, firstname: Giorgio, guid: 7d663c546bb811e3baa63085a99ccac7,
  lastname: Azzolini, modified: !!timestamp '2013-12-23 09:56:27', nationality: ITA,
  nickname: Oss, portrait: gazzolini.jpg}
rates: []
ratings:
- {default_deviation: 350, default_rate: 1500, default_volatility: '0.06', description: Test,
  guid: c08bc3da6c0011e389703085a99ccac7, level: C, modified: !!timestamp '2013-12-23
    18:33:43', tau: '0.5'}
seasons:
- {closed: true, club: 1, couplings: serial, description: Campionato SCR 1999-2000,
  guid: 7f8b7c4c6bb811e3baa63085a99ccac7, modified: !!timestamp '2013-12-23 09:56:30',
  playersperteam: 1, prizefactor: '55.55', prizes: fixed, skipworstprizes: 0}
---
competitors:
- bucholz: 39
  netscore: -16
  players: [1]
  points: 4
  prize: '2'
  totscore: 37
- bucholz: 12
  netscore: -42
  players: [2]
- bucholz: 30
  netscore: -3
  players: [3]
  points: 6
  prize: '6'
  totscore: 48
- bucholz: 35
  netscore: -33
  players: [4]
  points: 4
  prize: '1'
  totscore: 36
- bucholz: 45
  netscore: -24
  players: [5]
  points: 6
  prize: '9'
  totscore: 35
- bucholz: 26
  netscore: -87
  players: [6]
  totscore: 8
- bucholz: 35
  netscore: -54
  players: [7]
  points: 2
  totscore: 19
- bucholz: 36
  netscore: 42
  players: [8]
  points: 6
  prize: '8'
  totscore: 77
- bucholz: 36
  netscore: 3
  players: [9]
  points: 6
  prize: '7'
  totscore: 59
- bucholz: 30
  netscore: 12
  players: [10]
  points: 4
  totscore: 57
- bucholz: 41
  netscore: 17
  players: [11]
  points: 10
  prize: '16'
  totscore: 66
- bucholz: 37
  netscore: 70
  players: [12]
  points: 9
  prize: '14'
  totscore: 91
- bucholz: 22
  netscore: 14
  players: [13]
  points: 6
  prize: '3'
  totscore: 58
- bucholz: 43
  netscore: 39
  players: [14]
  points: 8
  prize: '12'
  totscore: 68
- bucholz: 47
  netscore: 36
  players: [15]
  points: 10
  prize: '18'
  totscore: 62
- bucholz: 14
  netscore: -33
  players: [16]
  points: 2
  totscore: 24
- bucholz: 28
  netscore: 9
  players: [17]
  points: 6
  prize: '4'
  totscore: 62
- bucholz: 30
  netscore: -7
  players: [18]
  points: 6
  prize: '5'
  totscore: 50
- bucholz: 39
  netscore: 4
  players: [19]
  points: 8
  prize: '10'
  totscore: 52
- bucholz: 49
  netscore: 41
  players: [20]
  points: 9
  prize: '13'
  totscore: 69
- bucholz: 42
  netscore: 12
  players: [21]
  points: 8
  prize: '11'
  totscore: 52
couplings: dazed
currentturn: 6
date: 2000-10-21
description: "5\xB0 Torneo"
duration: 45
guid: 808cdef66bb811e3baa63085a99ccac7
location: Pub La Torcia - Mori
matches:
- {board: 1, competitor1: 5, competitor2: 10, score1: 5, score2: 1, turn: 1}
- {board: 2, competitor1: 15, competitor2: 14, score1: 5, score2: 0, turn: 1}
- {board: 3, competitor1: 3, competitor2: 6, score1: 15, score2: 4, turn: 1}
- {board: 4, competitor1: 20, competitor2: 21, score1: 19, score2: 10, turn: 1}
- {board: 5, competitor1: 18, competitor2: 19, score1: 8, score2: 10, turn: 1}
- {board: 6, competitor1: 9, competitor2: 11, score1: 5, score2: 21, turn: 1}
- {board: 7, competitor1: 12, competitor2: 7, score1: 25, score2: 0, turn: 1}
- {board: 8, competitor1: 2, competitor2: 8, score1: 0, score2: 22, turn: 1}
- {board: 9, competitor1: 4, competitor2: 13, score1: 11, score2: 3, turn: 1}
- {board: 10, competitor1: 17, competitor2: 1, score1: 5, score2: 17, turn: 1}
- {board: 1, competitor1: 8, competitor2: 4, score1: 25, score2: 0, turn: 2}
- {board: 2, competitor1: 12, competitor2: 20, score1: 8, score2: 8, turn: 2}
- {board: 3, competitor1: 3, competitor2: 19, score1: 4, score2: 15, turn: 2}
- {board: 4, competitor1: 21, competitor2: 7, score1: 4, score2: 0, turn: 2}
- {board: 5, competitor1: 13, competitor2: 2, score1: 20, score2: 0, turn: 2}
- {board: 6, competitor1: 1, competitor2: 5, score1: 0, score2: 14, turn: 2}
- {board: 7, competitor1: 10, competitor2: 17, score1: 0, score2: 14, turn: 2}
- {board: 8, competitor1: 18, competitor2: 6, score1: 14, score2: 0, turn: 2}
- {board: 9, competitor1: 14, competitor2: 9, score1: 17, score2: 0, turn: 2}
- {board: 10, competitor1: 11, competitor2: 15, score1: 1, score2: 25, turn: 2}
- {board: 1, competitor1: 14, competitor2: 13, score1: 12, score2: 8, turn: 3}
- {board: 2, competitor1: 20, competitor2: 1, score1: 11, score2: 2, turn: 3}
- {board: 3, competitor1: 15, competitor2: 8, score1: 8, score2: 4, turn: 3}
- {board: 4, competitor1: 11, competitor2: 10, score1: 12, score2: 4, turn: 3}
- {board: 5, competitor1: 18, competitor2: 21, score1: 0, score2: 17, turn: 3}
- {board: 6, competitor1: 6, competitor2: 16, score1: 0, score2: 20, turn: 3}
- {board: 7, competitor1: 3, competitor2: 17, score1: 15, score2: 6, turn: 3}
- {board: 8, competitor1: 12, competitor2: 4, score1: 17, score2: 5, turn: 3}
- {board: 9, competitor1: 19, competitor2: 5, score1: 8, score2: 11, turn: 3}
- {board: 10, competitor1: 7, competitor2: 9, score1: 6, score2: 18, turn: 3}
- {board: 1, competitor1: 7, competitor2: 6, score1: 8, score2: 4, turn: 4}
- {board: 2, competitor1: 11, competitor2: 3, score1: 10, score2: 5, turn: 4}
- {board: 3, competitor1: 20, competitor2: 19, score1: 10, score2: 1, turn: 4}
- {board: 4, competitor1: 9, competitor2: 10, score1: 14, score2: 5, turn: 4}
- {board: 5, competitor1: 8, competitor2: 21, score1: 6, score2: 9, turn: 4}
- {board: 6, competitor1: 18, competitor2: 16, score1: 22, score2: 0, turn: 4}
- {board: 7, competitor1: 15, competitor2: 5, score1: 13, score2: 0, turn: 4}
- {board: 8, competitor1: 4, competitor2: 17, score1: 5, score2: 14, turn: 4}
- {board: 9, competitor1: 1, competitor2: 13, score1: 2, score2: 6, turn: 4}
- {board: 10, competitor1: 12, competitor2: 14, score1: 15, score2: 1, turn: 4}
- {board: 1, competitor1: 4, competitor2: 16, score1: 10, score2: 4, turn: 5}
- {board: 2, competitor1: 8, competitor2: 13, score1: 19, score2: 5, turn: 5}
- {board: 3, competitor1: 12, competitor2: 21, score1: 4, score2: 7, turn: 5}
- {board: 4, competitor1: 11, competitor2: 5, score1: 15, score2: 5, turn: 5}
- {board: 5, competitor1: 19, competitor2: 9, score1: 7, score2: 5, turn: 5}
- {board: 6, competitor1: 17, competitor2: 7, score1: 13, score2: 5, turn: 5}
- {board: 7, competitor1: 10, competitor2: 6, score1: 22, score2: 0, turn: 5}
- {board: 8, competitor1: 14, competitor2: 18, score1: 25, score2: 0, turn: 5}
- {board: 9, competitor1: 3, competitor2: 1, score1: 0, score2: 16, turn: 5}
- {board: 10, competitor1: 15, competitor2: 20, score1: 0, score2: 16, turn: 5}
- {board: 1, competitor1: 13, competitor2: 6, score1: 16, score2: 0, turn: 6}
- {board: 2, competitor1: 1, competitor2: 9, score1: 0, score2: 17, turn: 6}
- {board: 3, competitor1: 3, competitor2: 7, score1: 9, score2: 0, turn: 6}
- {board: 4, competitor1: 4, competitor2: 18, score1: 5, score2: 6, turn: 6}
- {board: 5, competitor1: 19, competitor2: 17, score1: 11, score2: 10, turn: 6}
- {board: 6, competitor1: 20, competitor2: 11, score1: 5, score2: 7, turn: 6}
- {board: 7, competitor1: 15, competitor2: 21, score1: 11, score2: 5, turn: 6}
- {board: 8, competitor1: 12, competitor2: 5, score1: 22, score2: 0, turn: 6}
- {board: 9, competitor1: 14, competitor2: 8, score1: 13, score2: 1, turn: 6}
- {board: 10, competitor1: 10, competitor2: 16, score1: 25, score2: 0, turn: 6}
modified: 2013-12-23 18:34:44
prealarm: 5
prized: true
prizefactor: '1'
rankedturn: 6
rating: 1
season: 1
