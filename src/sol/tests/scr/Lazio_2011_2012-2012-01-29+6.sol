championships: []
clubs:
- {couplings: dazed, description: Federazione Italiana Carrom, emblem: fic.png, guid: a67d9b0c6af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: ITA, prizes: fixed, siteurl: 'http://www.carromitaly.com/'}
- {couplings: serial, description: EuroCup, guid: a67eff386af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', prizes: fixed}
- {couplings: serial, description: Czech Carrom Association, guid: a6809fb46af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:12', nationality: CZE, prizes: fixed, siteurl: www.carrom.cz}
players:
- {citizenship: true, club: 1, firstname: Rodolfo, guid: a692707c6af811e3bee53085a99ccac7,
  lastname: Donnini, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: rodolfo.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Ari, guid: a69307626af811e3bee53085a99ccac7,
  lastname: Liyanage, modified: !!timestamp '2013-12-22 11:03:13', nationality: LKA,
  portrait: ary.jpg}
- {citizenship: true, club: 1, firstname: Paolo, guid: a69399526af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: Paolomart.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Stefano, guid: a695f60c6af811e3bee53085a99ccac7,
  lastname: Stucchi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  portrait: Stucchi.jpg, sex: M}
- {citizenship: true, club: 1, firstname: Lal, guid: a69688426af811e3bee53085a99ccac7,
  lastname: Rodrigo, modified: !!timestamp '2013-12-22 11:03:13', nationality: LKA,
  portrait: lal.jpg}
- {citizenship: true, club: 1, firstname: Amitha, guid: a6a942ac6af811e3bee53085a99ccac7,
  lastname: Ubhayathunga, modified: !!timestamp '2013-12-22 11:03:13', nationality: LKA,
  portrait: amitha.jpg}
- {citizenship: true, club: 1, firstname: Jude, guid: a6aafde06af811e3bee53085a99ccac7,
  lastname: Fernando, modified: !!timestamp '2013-12-22 11:03:13', nationality: LKA,
  portrait: jude.jpg}
- {citizenship: true, club: 1, firstname: Elisa, guid: a6b21d326af811e3bee53085a99ccac7,
  lastname: Zucchiatti, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  nickname: elisa, portrait: elisa.jpg, sex: F}
- {citizenship: true, club: 1, firstname: Patrick, guid: a70b8b9c6af811e3bee53085a99ccac7,
  lastname: Philips, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA}
- {citizenship: true, club: 1, firstname: Giorgio, guid: a71151bc6af811e3bee53085a99ccac7,
  lastname: Balicchi, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  sex: M}
- {citizenship: true, club: 1, firstname: Francesco, guid: a718d4506af811e3bee53085a99ccac7,
  lastname: Pedicini, modified: !!timestamp '2013-12-22 11:03:13', nationality: ITA,
  sex: M}
- {citizenship: true, club: 1, firstname: Sumith, guid: a750c7b66af811e3bee53085a99ccac7,
  lastname: Wickrama, modified: !!timestamp '2013-12-22 11:03:14', nationality: LKA}
- {citizenship: true, club: 1, firstname: Wasantha, guid: a751f42e6af811e3bee53085a99ccac7,
  lastname: Welikumbura, modified: !!timestamp '2013-12-22 11:03:14', nationality: LKA}
- {citizenship: true, club: 2, firstname: Andrea, guid: a7ce242c6af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:15', nationality: ITA,
  sex: M}
- {citizenship: true, club: 1, firstname: Lucia Elena, guid: a7fe84a06af811e3bee53085a99ccac7,
  lastname: Ammazzalamorte, modified: !!timestamp '2013-12-22 11:03:15', nationality: ITA,
  sex: F}
- {citizenship: true, firstname: Horst, guid: a7eecaf66af811e3bee53085a99ccac7, lastname: Simunsky,
  modified: !!timestamp '2013-12-22 11:03:15', nationality: CZE}
- {citizenship: true, club: 3, firstname: Pavel, guid: a7ef5e3a6af811e3bee53085a99ccac7,
  lastname: Schierl, modified: !!timestamp '2013-12-22 11:03:15', nationality: CZE,
  sex: M}
- {citizenship: true, club: 2, firstname: Giulio, guid: a82bacc86af811e3bee53085a99ccac7,
  lastname: Martinelli, modified: !!timestamp '2013-12-22 11:03:15', nationality: ITA,
  sex: M}
rates: []
ratings: []
seasons:
- {closed: true, club: 1, couplings: serial, description: Lazio 2011-2012, guid: a926de046af811e3bee53085a99ccac7,
  modified: !!timestamp '2013-12-22 11:03:17', playersperteam: 1, prizefactor: '0',
  prizes: millesimal, skipworstprizes: 1}
---
competitors:
- bucholz: 42
  netscore: -58
  players: [1]
  points: 4
  prize: '285'
  totscore: 52
- bucholz: 41
  netscore: 41
  players: [2]
  points: 6
  prize: '615'
  totscore: 101
- bucholz: 49
  players: [3]
  points: 7
  prize: '670'
  totscore: 88
- bucholz: 34
  netscore: 40
  players: [4]
  points: 8
  prize: '780'
  totscore: 120
- bucholz: 45
  netscore: 25
  players: [5]
  points: 8
  prize: '890'
  totscore: 114
- bucholz: 40
  netscore: 110
  players: [6]
  points: 12
  prize: '1000'
  totscore: 147
- bucholz: 34
  netscore: -40
  players: [7]
  points: 2
  prize: '175'
  totscore: 55
- bucholz: 44
  netscore: 26
  players: [8]
  points: 8
  prize: '835'
  totscore: 110
- bucholz: 37
  netscore: 38
  players: [9]
  points: 6
  prize: '505'
  totscore: 98
- bucholz: 30
  netscore: -2
  players: [10]
  points: 4
  prize: '230'
  totscore: 90
- bucholz: 39
  netscore: 49
  players: [11]
  points: 9
  prize: '945'
  totscore: 123
- bucholz: 22
  netscore: 20
  players: [12]
  points: 8
  prize: '725'
  totscore: 88
- bucholz: 39
  netscore: -11
  players: [13]
  points: 6
  prize: '560'
  totscore: 81
- bucholz: 33
  netscore: -21
  players: [14]
  points: 6
  prize: '395'
  totscore: 66
- bucholz: 31
  netscore: -116
  players: [15]
  prize: '65'
  totscore: 13
- bucholz: 34
  netscore: -26
  players: [16]
  points: 6
  prize: '450'
  totscore: 59
- bucholz: 26
  netscore: -97
  players: [17]
  points: 2
  prize: '120'
  totscore: 21
- bucholz: 28
  netscore: 22
  players: [18]
  points: 6
  prize: '340'
  totscore: 104
couplings: serial
currentturn: 6
date: 2012-01-29
description: 2 tappa Roma
duration: 45
guid: b0f85c206af811e3bee53085a99ccac7
location: Esquilino
matches:
- {board: 1, competitor1: 8, competitor2: 4, score1: 24, score2: 17, turn: 1}
- {board: 2, competitor1: 12, competitor2: 9, score1: 0, score2: 25, turn: 1}
- {board: 3, competitor1: 10, competitor2: 13, score1: 18, score2: 21, turn: 1}
- {board: 4, competitor1: 2, competitor2: 6, score1: 7, score2: 25, turn: 1}
- {board: 5, competitor1: 7, competitor2: 16, score1: 5, score2: 10, turn: 1}
- {board: 6, competitor1: 15, competitor2: 11, score1: 0, score2: 25, turn: 1}
- {board: 7, competitor1: 18, competitor2: 5, score1: 4, score2: 25, turn: 1}
- {board: 8, competitor1: 14, competitor2: 3, score1: 0, score2: 25, turn: 1}
- {board: 9, competitor1: 1, competitor2: 17, score1: 13, score2: 4, turn: 1}
- {board: 1, competitor1: 9, competitor2: 11, score1: 14, score2: 24, turn: 2}
- {board: 2, competitor1: 3, competitor2: 5, score1: 25, score2: 13, turn: 2}
- {board: 3, competitor1: 6, competitor2: 1, score1: 25, score2: 2, turn: 2}
- {board: 4, competitor1: 8, competitor2: 16, score1: 25, score2: 0, turn: 2}
- {board: 5, competitor1: 10, competitor2: 4, score1: 0, score2: 25, turn: 2}
- {board: 6, competitor1: 13, competitor2: 7, score1: 25, score2: 0, turn: 2}
- {board: 7, competitor1: 17, competitor2: 2, score1: 0, score2: 25, turn: 2}
- {board: 8, competitor1: 12, competitor2: 15, score1: 16, score2: 5, turn: 2}
- {board: 9, competitor1: 18, competitor2: 14, score1: 12, score2: 23, turn: 2}
- {board: 1, competitor1: 6, competitor2: 3, score1: 25, score2: 0, turn: 3}
- {board: 2, competitor1: 8, competitor2: 11, score1: 20, score2: 20, turn: 3}
- {board: 3, competitor1: 4, competitor2: 5, score1: 9, score2: 25, turn: 3}
- {board: 4, competitor1: 13, competitor2: 9, score1: 11, score2: 8, turn: 3}
- {board: 5, competitor1: 2, competitor2: 14, score1: 25, score2: 0, turn: 3}
- {board: 6, competitor1: 1, competitor2: 16, score1: 17, score2: 13, turn: 3}
- {board: 7, competitor1: 12, competitor2: 10, score1: 21, score2: 11, turn: 3}
- {board: 8, competitor1: 18, competitor2: 17, score1: 25, score2: 0, turn: 3}
- {board: 9, competitor1: 7, competitor2: 15, score1: 25, score2: 0, turn: 3}
- {board: 1, competitor1: 5, competitor2: 1, score1: 25, score2: 15, turn: 4}
- {board: 2, competitor1: 6, competitor2: 13, score1: 22, score2: 11, turn: 4}
- {board: 3, competitor1: 8, competitor2: 3, score1: 18, score2: 18, turn: 4}
- {board: 4, competitor1: 4, competitor2: 7, score1: 25, score2: 8, turn: 4}
- {board: 5, competitor1: 18, competitor2: 10, score1: 21, score2: 14, turn: 4}
- {board: 6, competitor1: 9, competitor2: 14, score1: 7, score2: 15, turn: 4}
- {board: 7, competitor1: 15, competitor2: 17, score1: 8, score2: 13, turn: 4}
- {board: 8, competitor1: 11, competitor2: 2, score1: 20, score2: 15, turn: 4}
- {board: 9, competitor1: 12, competitor2: 16, score1: 12, score2: 17, turn: 4}
- {board: 1, competitor1: 16, competitor2: 14, score1: 9, score2: 7, turn: 5}
- {board: 2, competitor1: 2, competitor2: 1, score1: 18, score2: 3, turn: 5}
- {board: 3, competitor1: 4, competitor2: 18, score1: 20, score2: 17, turn: 5}
- {board: 4, competitor1: 12, competitor2: 7, score1: 14, score2: 8, turn: 5}
- {board: 5, competitor1: 6, competitor2: 11, score1: 25, score2: 9, turn: 5}
- {board: 6, competitor1: 8, competitor2: 5, score1: 11, score2: 18, turn: 5}
- {board: 7, competitor1: 13, competitor2: 3, score1: 7, score2: 20, turn: 5}
- {board: 8, competitor1: 9, competitor2: 17, score1: 25, score2: 0, turn: 5}
- {board: 9, competitor1: 10, competitor2: 15, score1: 25, score2: 0, turn: 5}
- {board: 1, competitor1: 13, competitor2: 4, score1: 6, score2: 24, turn: 6}
- {board: 2, competitor1: 14, competitor2: 7, score1: 21, score2: 9, turn: 6}
- {board: 3, competitor1: 16, competitor2: 9, score1: 10, score2: 19, turn: 6}
- {board: 4, competitor1: 3, competitor2: 11, score1: 0, score2: 25, turn: 6}
- {board: 5, competitor1: 12, competitor2: 1, score1: 25, score2: 2, turn: 6}
- {board: 6, competitor1: 18, competitor2: 15, score1: 25, score2: 0, turn: 6}
- {board: 7, competitor1: 10, competitor2: 17, score1: 22, score2: 4, turn: 6}
- {board: 8, competitor1: 6, competitor2: 5, score1: 25, score2: 8, turn: 6}
- {board: 9, competitor1: 8, competitor2: 2, score1: 12, score2: 11, turn: 6}
modified: 2013-12-22 11:03:30
prealarm: 5
prized: true
prizefactor: '1'
prizes: millesimal
rankedturn: 6
season: 1
